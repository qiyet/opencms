# opencms

#### 介绍

基于thinkphp的一个简单的博客后台，采用最小的单应用模式，去除臃肿的模块代码，采用php原生模板驱动，直接使用php语法就可以进行开发，不用二次学习框架语法，节省了不必要的时间成本，只有不到2M的源码包，在几块钱的虚拟主机上就可以流畅的跑起来，节约了个人或者企业网站建设运营成本。由[开发编程博客](http://codeit.org.cn) 自主开发。

#### 软件架构

基于thinkphp的一个cms，可以做个人博客、企业官网、app和小程序后台等，去除一般cms的花里胡哨的冗余代码，非常简约轻量，但是功能强大。
争取做简单实用的CMS，让每个人都能轻松使用互联网技术实现增值效应。



#### 安装教程

1.  下载本源码
2.  把123文件夹下面的sql导入
3.  修改配置和伪静态
#### 功能介绍

1.  无限极分类
2.  单页
3.  系统配置

其他的模块：淘宝客系统、图书系统、crm等暂时不开源，以后陆续开源。
目前开源的模块是一个后台，用户登录验证部分正在开发...

####  **免责声明** 

1.  该程序可以免费使用，也可以用于任何合法的商业用途，可以自行二次开发和学习研究交流。
2.  使用该源码导致的损失，与本人无关，
3.  不得用于任何非法用途，


#### 使用说明

1.  如果测试学习，可以在本地搭建php环境，支持php7.4
2.  如果线上环境测试，可以选择1元php空间 ，具体参考 [学习用虚拟主机开通指南](http://codeit.org.cn/123/vhost.html) ,不用备案，上传上去就可以使用。非常适合学习开发用
3.  代码正在上传中..

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
