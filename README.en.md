# opencms

#### Description
基于thinkphp的一个简单的博客后台，采用最小的单应用模式，去除臃肿的模块，采用php原生模板驱动，直接使用php语法就可以进行开发，不用二次学习框架语法，只有不到2M的源码包，在几块钱的虚拟主机上就可以流畅的跑起来，节约了企业成本。由开发编程博客 codeit.org.cn 自主开发。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
