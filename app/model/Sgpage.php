<?php
namespace app\model;
use think\Model;
		/*** 单页管理模型*/
class Sgpage extends Model
    {
		 //protected $table = 'category';//指定的表,模型会自动对应数据表，模型类的命名规则是除去表前缀的数据表名称，采用驼峰法命名，并且首字母大写
		//默认主键为id,k如果设置其他：protected $pk = 'uid';
		// 设置当前模型对应的完整数据表名称
		/*protected $table = 'think_user'; //设置当前模型的数据库连接
		protected $connection = 'db_config';*/
		//protected $autoWriteTimestamp = 'int';
		// 设置字段信息,在模型中明确定义字段信息避免多一次查询的开销。
		protected $schema = [
			'id'          => 'int',
			'cid'         => 'smallint',
			//'typeid'      => 'smallint',
			'title'       => 'varchar',
			'subtitle'    => 'varchar',
			//'source'      => 'varchar',
			'description' => 'varchar',
			'image' 	  => 'varchar',
			'content' 	  => 'text',
			//'tags' 	  	  => 'varchar',
			//'istop'        => 'tinyint',
			'isdel'        => 'tinyint',
			//'visible'        => 'tinyint',
			'create_time' => 'int',
			'update_time' => 'int',
			'click' 	=> 'int',
			
		];

		
	

    }