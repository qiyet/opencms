<?php
namespace app\controller;

use app\BaseController;

class Index extends BaseController
{
    public function index()
    {
        return \think\facade\View::fetch();
    }
    
    public function hello($name = 'ThinkPHP6')
    {
        return 'ThinkPHP 版本V' . \think\facade\App::version() .'hello,' . $name;
    }
}
