<?php
namespace app\controller\admin;

use app\BaseController;
use app\model\Tags;
use think\facade\View;
class Tag extends BaseController
{
    
    public function index()
    {
        $tags = Tags::where('status', 1)->select()->toArray();
        //var_dump($tags);exit;
        View::assign('tags', $tags);
        return View::fetch('index');
    }
    // 添加标签
    public function add(){
        $str=input('post.tnames');
        if(empty($str)){
            //加载模板
            return View::fetch();
           //echo '标签名不能为空';
           // return false;
        }else{
            $str=nl2br(trim($str));
            $tnames=explode("<br />", $str);
            foreach ($tnames as $k => $v) {
                $v=trim($v);
                if(!empty($v)){
                    $data['name']=$v;
                    $tag = new Tags;
                    $tag->save($data);
                   echo  $tag->id?'成功':'失败,联系codeit.org.cn';
                }
            }
            echo "end";
            //return true;
        }

    }

    //编辑标签
    public function edit(){
        if(input('post.')){
            $data=input('post.');
            //var_dump($data);exit;
            if(Tags::update($data)){ echo '修改成功 update success!'; } else{echo '更新出现错误 联系bd789.cn';}
        }else{
            //初次加载，获得标签
            $tid=input('get.tid');
            $onedata=Tags::find($tid);//根据主键id获得一条数据
            //var_dump($onedata);exit;
            view::assign('onedata',$onedata);
            return View::fetch();

        }
    }
     // 删除标签
     public function delete(){
        $tid=input('get.tid',0,'intval');
        
        if(Tags::update(['status' => 0], ['id' => $tid])){ echo '软删除成功   success!'; }
    }
}
