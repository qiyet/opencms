<?php
namespace app\controller\admin;

use app\BaseController;
use app\model\Category;
use app\model\Articles;
use think\facade\View;
//use think\facade\Db;
class Article extends BaseController
{
    
    public function index()
    {
        //$articles = Articles::where('isdel', 0)->select()->toArray();
        
        /*用db和model 均可以实现分页，用db的使用方法：//$articles = Db::name('Articles')->where('isdel', 0)->paginate(3);
        *用mode 的分页方法，会自动转换成数组
        */ 
        $articles = Articles::where('isdel', 0)->order('update_time', 'desc')->paginate(10);
        $pages =$articles->render();  //分页
        //var_dump($articles);exit;
        View::assign('data', $articles);
        View::assign('page', $pages);
        return View::fetch('index');
    }
    // 添加文章
    public function add(){
        $str=input('post.');
        if(empty($str)){
            $clist = Category::select()->toArray();
            $clist=$this->sort($clist);
            view::assign('data',$clist);
            //加载模板
            return View::fetch();
         
        }else{
//还可以直接静态调用create方法创建并写入
        $article = Articles::create($str);
        echo $article ->id;// 打印id等参数
      if($article){
        $this->baidu_site($article->id);//向百度主动推送资源，API提交
         // echo 'ddd';
      }
        }

    }

    // 向同步百度推送 发布需要用变量代替api，让用户方便管理,需要替换掉网站@site 和token，否则，提交不成功
    function baidu_site($aid){
        $urls = array(
            $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["SERVER_NAME"].'/a/'.$aid.'.html',
            'http://codeit.org.cn/123/vhost.html',
        );
        $api = 'http://data.zz.baidu.com/urls?site=https://www.bd789.cn&token=你的token';
        //$api=C('BAIDU_SITE_URL');
        $ch=curl_init();
        $options=array(
            CURLOPT_URL=>$api,
            CURLOPT_POST=>true,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_POSTFIELDS=>implode("\n", $urls),
            CURLOPT_HTTPHEADER=>array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result=curl_exec($ch);
         /*var_dump($result);exit;
         $msg=json_decode($result,true);
          if($msg['code']==500){curl_exec($ch);} */
        curl_close($ch);
    }


    //编辑文章
    public function edit(){
        if(input('post.')){
            $data=input('post.');
            //var_dump($data);exit;
            if(Articles::update($data)){ echo '修改成功 update success!'; } else{echo '更新出现错误 联系codeit.org.cn';}
        }else{
            //初次加载，获得标签
            $id=input('get.id');
            $onedata=Articles::find($id);//根据主键id获得一条数据
            //var_dump($onedata);exit;
            $clist = Category::select()->toArray();//获得所有分类，用于选择分类
            $clist=$this->sort($clist);//格式化处理
            view::assign('data',$clist);
            view::assign('onedata',$onedata);
            return View::fetch();

        }
    }
     // 软删除文章，放入回收站delete
     public function isdel(){
        $id=input('get.id',0,'intval');
        
        if(Articles::update(['isdel' =>1], ['id' => $id])){ echo '软删除成功   success!'; }
    }

    /**
 * 无限极分类及格式化输出,具体用法 blog.csdn.net/qikexun/article/details/130013327
    * @param Array $data     //数据库里获取的结果集
     * @param Int $pid
     * @param Int $count       //第几级分类
     * @return Array $treeList  used by http://vcncn.cn
*/
function sort($data,$pid=0,$level=0){
    //此处数据必须是静态数组，不然递归的时候每次都会声明一个新的数
    static $arr = array();
    foreach ($data as $key=>$value){
        if($value['pid'] == $pid){
            $value["level"]=$level;
            $arr[]=$value;
            $this->sort($data,$value['id'],$level+1);
        }
    }
    return $arr;
}


}
