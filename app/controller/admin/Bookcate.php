<?php
namespace app\controller\admin;

use app\BaseController;
use app\model\Bookclass; 
use think\facade\View;
class Bookcate extends BaseController
{
    /* //定义数据表，这种方法也可以实现，但是，直接用模型更简单
    private $category; */
    //定义category 数据
  /*   private $categoryData;
    //构造函数 实例化CategoryModel 并获得整张表的数据
    public function __construct(){
    
      //初始化时实例化category model
        $this->category = new Category; 
        //获取category数据并赋值给$categoryData
        $this->categoryData=$this->category::select();
    } */
    public function index()
    {
        $clist = Bookclass::select()->toArray();
       // $clist=$this->list_to_tree($clist);
       $clist=$this->sort($clist);
       // var_dump($clist);exit;
         view::assign('data',$clist);
        return \think\facade\View::fetch();
    }
    public function add()
    {
       /* $category = new Category;
        $category->save([
    'name'  =>  'thinkphp',
    'email' =>  'thinkphp@qq.com'
]);*/

if(input('?post.name')){
//var_dump(input('param.'));exit;  // var_dump($data);exit; 这两个效果一样，
   $category = new Bookclass;
    //$category->name = input('post.cname');
    $data=input('post.');
 
   $category->save($data);
    if($category->id){
         echo 'add success!/添加分类成功';
        // 获取自增ID
        echo $category->id;
    }else{
         echo 'add 数据插入失败，联系codeit.org.cn';
        }
    }else{
        
        $cid=input('get.cid',0,'intval');
        if(!$cid){
            $cid=1;
         } view::assign('cid',$cid);
         /*var_dump($this->categoryData);
         $list=$this->categoryData;
         foreach($list as $key=>$user){
            echo $user->name;
            // 根据主键获取多个数据

        }
        //echo $this->categoryData["name"];*/
        $clist = Bookclass::select()->toArray();
       // $clist=$this->list_to_tree($clist);
       $clist=$this->sort($clist);
       // var_dump($clist);exit;
         view::assign('data',$clist);
         /* view::display();//需要传入参数 */
         return View::fetch();
}

    }
    public function edit(){
        $data=input('post.');//接受post 数组
        if($data){ /* 有参数传来，则模型更新，第一个参数中包含主键数据，可以无需传入第二个参数['id' => 1]（更新条件） */
            if(Bookclass::update($data)){ echo '修改成功 update success!'; } else{echo '更新出现错误 联系vcncn.cn';}

        }else{ //没有参数，则根据主键获取数据
            $cid=input('get.cid',0,'intval');
            $onedata=Bookclass::find($cid);;//根据主键id获得一条数据
            //var_dump($onedata);
            $clist = Bookclass::select()->toArray();//获得所有分类，用于选择分类
            $clist=$this->sort($clist);//格式化处理
            view::assign('data',$clist);
            view::assign('onedata',$onedata);
            //view::assign('cid',$cid);
            return View::fetch();

        }
       

    }
    // 删除分类
    public function delete(){
        $cid=input('get.cid',0,'intval');
        if(Bookclass::destroy($cid)){echo 	'删除成功 delete success!';} else{echo '删除出现错误 联系vcncn.cn';}
    }

    /**
     * 排序
     */
    public function bysort(){
        echo '可以通过sort 字段批量修改参数来实现排序，或者其他的排序，预留此方法 for different sorting needs.';

    }
//test by sunseek.top
    public function hello($name = 'ThinkPHP6')
    {
        return 'ThinkPHP 版本V' . \think\facade\App::version() .'hello,' . $name;
    }


/**
 * 无限极分类及格式化输出,具体用法 blog.csdn.net/qikexun/article/details/130013327
    * @param Array $data     //数据库里获取的结果集
     * @param Int $pid
     * @param Int $count       //第几级分类
     * @return Array $treeList  used by http://vcncn.cn
*/
    function sort($data,$pid=0,$level=0){
        //此处数据必须是静态数组，不然递归的时候每次都会声明一个新的数
        static $arr = array();
        foreach ($data as $key=>$value){
            if($value['pid'] == $pid){
                $value["level"]=$level;
                $arr[]=$value;
                $this->sort($data,$value['id'],$level+1);
            }
        }
        return $arr;
    }

    /**
     * *OneThink中无限极分类函数,输出是有_chile的数组,前端输出处理需要递归判断
 * 把返回的数据集转换成Tree
 * @param array $list 要转换的数据集
 * @param string $pid parent标记字段
 * @param string $level level标记字段
 * @return array
 * @author 麦当苗儿 < codeit.org.cn >
 */
function list_to_tree($list, $pk='id', $pid = 'pid', $child = '_child', $root = 0) {
    // 创建Tree
    $tree = array();
    if(is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }

        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId =  $data[$pid];
            if ($root == $parentId) {
                $tree[] =& $list[$key];
            }else{
                if (isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    return $tree;
}
}
