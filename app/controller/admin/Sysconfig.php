<?php
namespace app\controller\admin;

use app\BaseController;
use app\model\Configu;
use think\facade\View;
class Sysconfig extends BaseController
{
    
    public function index()
    {
        $Configu = Configu::where('status', 1)->select()->toArray();
        //var_dump($tags);exit;
        View::assign('Configu', $Configu);
        return View::fetch('index');
    }
    // 添加
    public function add(){
        $str=input('post.tnames');
       
        return true;
       

    }

    //编辑
    public function edit(){
        if(input('post.')){
            $data=input('post.');
            //var_dump($data);exit;
            foreach ($data as $k =>$v) {
//save方法更新数据，只会更新变化的数据，对于没有变化的数据是不会进行重新更新的,
                $Configu = Configu::where('status',1)->where('name',$k)->find();
                $Configu->name = $k;
                $Configu->value = $v;
                if($Configu->save()){ echo '更新成功~';}
                /* if(Configu::update(['name' => $k], ['value' => $v])){ echo '修改成功 update success!'; } else{echo '更新出现错误 联系bd789.cn';} */
                $data[$k]=htmlspecialchars_decode($v);
            }
            //$data['WEB_STATISTICS']=str_replace( "'",'"',$data['WEB_STATISTICS']);
            $str=<<<EOF
<?php
return array(
   /*此配置项为自动生成,如果直接修改 ，则后台网站设置中提交后，会更新到数据库中.如果填入数据库中没有的选项，更新的时候会报错*/
   //网站设置
   'web_name' => '{$data['web_name']}', // 网站名称
   'web_basehost' => '{$data['web_basehost']}', // 网站主域名
   'web_icp' => '{$data['web_icp']}',  // 网站ICP备案号
   //weo优化推广
   'web_keywords' => '{$data['web_keywords']}', // seo 关键词
   'web_description' => '{$data['web_description']}',// 网站描述介绍
   //第三方api接口
   'baidu_site_url' => '{$data['baidu_site_url']}', //向百度推送的url
   'baidu_token' => '{$data['baidu_token']}', //百度推送的token
);
EOF;
file_put_contents(config_path().'webconfig.php', $str);
           
        }else{
            //初次加载，获得配置项 $Configu = Configu::where('status', 1)->select()->toArray();
            $Configu= \think\facade\Config::get('webconfig');
       //var_dump($Configu);exit;
        View::assign('data', $Configu);
            return View::fetch();

        }
    }
     // 删除
     public function delete(){
        $tid=input('get.tid',0,'intval');
        
        if(Configu::update(['status' => 0], ['id' => $tid])){ echo '软删除成功   success!'; }
    }
}
