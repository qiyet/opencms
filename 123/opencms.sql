-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2023-05-13 18:15:10
-- 服务器版本： 5.7.26
-- PHP 版本： 5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `opencms`
--

-- --------------------------------------------------------

--
-- 表的结构 `ci_articles`
--

CREATE TABLE `ci_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `cid` smallint(5) NOT NULL DEFAULT '0' COMMENT '分类id',
  `typeid` smallint(5) NOT NULL DEFAULT '0' COMMENT '栏目id 副栏目',
  `title` char(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `source` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '来源,1表示原创',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '摘要',
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '缩略图封面图',
  `content` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  `tags` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '标签，多个，可用于keywords',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶',
  `visible` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示，1显示',
  `isdel` tinyint(1) NOT NULL DEFAULT '0' COMMENT '软删除，1 删除',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `mid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '发布者id',
  `audit` mediumint(8) NOT NULL DEFAULT '0' COMMENT '审核员id',
  `click` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '点击数',
  `rank` tinyint(1) DEFAULT NULL COMMENT '浏览权限',
  `scores` tinyint(3) NOT NULL DEFAULT '0' COMMENT '消耗积分',
  `good` smallint(6) NOT NULL DEFAULT '0' COMMENT '好评',
  `bad` tinyint(3) NOT NULL DEFAULT '0' COMMENT '差评 badpost'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_articles`
--

INSERT INTO `ci_articles` (`id`, `cid`, `typeid`, `title`, `author`, `source`, `description`, `image`, `content`, `tags`, `istop`, `visible`, `isdel`, `create_time`, `update_time`, `mid`, `audit`, `click`, `rank`, `scores`, `good`, `bad`) VALUES
(1, 6, 0, '地方', '0', NULL, '随风倒士大夫', NULL, '史丹福', '史丹福', 0, 0, 0, 1682334211, 1682334211, 0, 0, 0, NULL, 0, 0, 0),
(2, 6, 0, 'sfsdf', '0', NULL, 'sdfsdfsf', NULL, 'sdfsdfs', 'dff', 0, 0, 0, 1682477055, 1682477055, 0, 0, 0, NULL, 0, 0, 0),
(3, 6, 0, 'uuu', '0', NULL, 'rtyryryr', NULL, 'ryeryeyeryt', 'uuty', 0, 0, 0, 1682477147, 1682477147, 0, 0, 0, NULL, 0, 0, 0),
(4, 6, 0, 'seew', '0', NULL, 'werwer', NULL, 'werwer', 'we', 0, 0, 0, 1682477181, 1682477181, 0, 0, 0, NULL, 0, 0, 0),
(5, 6, 0, 't推送', '0', NULL, '推送', NULL, '推送', '百度', 0, 0, 1, 1682477378, 1683182742, 0, 0, 0, NULL, 0, 0, 0),
(6, 6, 0, '儿童修改测试', '0', NULL, '他依然天涯555', NULL, '而特特特撒发顺丰55555', 'ert，555', 1, 1, 1, 1682477432, 1682592896, 0, 0, 0, NULL, 0, 0, 0),
(7, 6, 0, '有引用', '0', NULL, '一台', NULL, ' 原图原图', '一天天', 0, 0, 0, 1682477744, 1682477744, 0, 0, 0, NULL, 0, 0, 0),
(8, 6, 0, '她她她', '0', NULL, ' 头条 ', NULL, '他88888546546465555', '他', 0, 0, 1, 1682477867, 1682592927, 0, 0, 0, NULL, 0, 0, 0),
(9, 6, 0, '很好', '0', NULL, '哈哈哈哈哈', NULL, '哈哈哈', '很好很好', 0, 0, 0, 1682478007, 1682478007, 0, 0, 0, NULL, 0, 0, 0),
(10, 6, 0, '人人', '0', NULL, '人人人', NULL, '人人人', '人人', 0, 0, 0, 1682478028, 1682478028, 0, 0, 0, NULL, 0, 0, 0),
(11, 6, 0, '头条', '0', NULL, '她她她她她她她她她', NULL, '他吞吞吐吐', '她她她她她她', 0, 0, 1, 1682478122, 1682592919, 0, 0, 0, NULL, 0, 0, 0),
(12, 6, 0, '显示', '0', NULL, '显示', NULL, '显示是老款的99势力对抗肌肤', '显示', 0, 1, 0, 1682480266, 1682480266, 0, 0, 0, NULL, 0, 0, 0),
(13, 6, 0, 'sfsdfsfsfsfsa', '0', NULL, 'sdfsd', NULL, 'sfdsf', '1,2,3,4,e', 0, 1, 0, 1682585371, 1682585371, 0, 0, 0, NULL, 0, 0, 0),
(14, 9, 0, '子栏目', '0', NULL, '是否是否是', NULL, '撒发顺丰', '标签,标签2，标签3', 1, 1, 0, 1682585518, 1682585518, 0, 0, 0, NULL, 0, 0, 0),
(15, 6, 0, 'a标签', '0', NULL, 'html', NULL, '<a href=\"bd789.cn\">beidoiu</a>', 'html', 0, 1, 0, 1682591622, 1682591622, 0, 0, 0, NULL, 0, 0, 0),
(16, 11, 0, '默认1文章', '0', NULL, '这个是文章的描述', NULL, '文章的内容', '标签,吧，好，', 1, 1, 0, 1682593150, 1682593483, 0, 0, 0, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ci_bookclass`
--

CREATE TABLE `ci_bookclass` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `pid` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父栏目',
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(10) NOT NULL,
  `keywords` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `sort` tinyint(1) NOT NULL DEFAULT '0' COMMENT '排序'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_bookclass`
--

INSERT INTO `ci_bookclass` (`id`, `pid`, `name`, `create_time`, `update_time`, `keywords`, `description`, `sort`) VALUES
(1, 0, '图书分类1', 1683110782, 1683110940, '图书，小说', '图书分类描述测试，有问题联系 codeit.org.cn 获得技术支持', 1),
(2, 1, '2222', 1683110869, 1683442226, '小说', '描述', 1),
(3, 0, '测试分类', 1683795688, 1683795688, '测试', '测试小说分类', 1);

-- --------------------------------------------------------

--
-- 表的结构 `ci_books`
--

CREATE TABLE `ci_books` (
  `id` int(11) NOT NULL,
  `cid` smallint(6) NOT NULL DEFAULT '0' COMMENT '分类id',
  `name` varchar(46) COLLATE utf8_unicode_ci NOT NULL COMMENT '书名',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seo描述',
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT '图书介绍总结摘要等summary,abstract',
  `author` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '作者',
  `mid` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员id,作者id,发布人id',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=连载中，1=已完结',
  `pic` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '封面缩略图',
  `freenum` smallint(6) NOT NULL DEFAULT '0' COMMENT '免费的章节数',
  `hits` smallint(6) NOT NULL DEFAULT '0' COMMENT '浏览数',
  `rate` tinyint(1) UNSIGNED NOT NULL DEFAULT '9' COMMENT '评分,得分,满分10',
  `isdel` tinyint(1) NOT NULL DEFAULT '0' COMMENT '软删除：0正常,1删除',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐和置顶',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `word` smallint(6) NOT NULL COMMENT '字数',
  `good` smallint(6) NOT NULL DEFAULT '1' COMMENT '点赞数'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_books`
--

INSERT INTO `ci_books` (`id`, `cid`, `name`, `description`, `body`, `author`, `mid`, `status`, `pic`, `freenum`, `hits`, `rate`, `isdel`, `istop`, `create_time`, `update_time`, `word`, `good`) VALUES
(1, 1, 'codeit图书小说系统', 'seo 描述', '这个是图书的介绍页面，\r\n概括总结，技术支持 codeit.org.cn 开发者编程社区，2023年5月', 'codeit', 0, 0, '0', 0, 0, 9, 0, 0, 1539841781, 1683111354, 1000, 1),
(2, 1, '小说测试', '小说的测试添加', '小说测试添加内容', '0', 0, 0, '0', 0, 0, 9, 0, 0, 1683182469, 1683182469, 555, 1),
(5, 1, '小伙3', '小说3', '小说3内容发给删除', '0', 0, 0, '0', 0, 0, 9, 1, 0, 1683182564, 1683182806, 666, 1),
(6, 1, '重复测试', '的地方是', '受辐射', '0', 0, 0, '0', 0, 0, 9, 0, 0, 1683186208, 1683186208, 99, 1),
(7, 1, '重复测试', '是否', '是否', '0', 0, 0, '0', 0, 0, 9, 1, 0, 1683186230, 1683190109, 666, 1),
(8, 1, '小说添加', '胜多负少', '史丹福', '0', 0, 0, '0', 0, 0, 9, 0, 0, 1683359429, 1683359429, 654, 1),
(9, 1, '图书修改', '修改', '修改', '0', 0, 0, '0', 0, 0, 9, 1, 0, 1683365609, 1683795623, 0, 1),
(10, 3, '测试小说', '测试图书', '测试图书的内容', '0', 0, 0, '0', 0, 0, 9, 0, 0, 1683795718, 1683795718, 500, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ci_category`
--

CREATE TABLE `ci_category` (
  `id` smallint(5) NOT NULL,
  `pid` smallint(5) NOT NULL DEFAULT '0' COMMENT '父栏目',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `keywords` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '分类描述',
  `sort` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_category`
--

INSERT INTO `ci_category` (`id`, `pid`, `name`, `create_time`, `update_time`, `keywords`, `description`, `sort`) VALUES
(6, 0, '时间888', 1680780222, 1680780222, '世界的离开家里', '手动阀手动阀', 5),
(7, 0, '不定义', 1680780288, 1680780288, '不定义', '不定义模型', 5),
(8, 0, 'z测试分类', 1680781680, 1680781680, '分类测试', '描述内容', 6),
(9, 6, '子栏目88', 1680853832, 1680853832, '子栏目', '子栏目时间888', 5),
(16, 0, '默认 1', 1680956655, 1680956655, '默认排序', '默认排序1', 1),
(11, 7, '子不定义', 1680853860, 1680955413, '子栏目', '修改不定义子栏目', 5),
(12, 0, '分类一', 1680857591, 1680857591, '顶级一', '这个是分类一', 5),
(13, 12, '分类一1', 1680857650, 1680857650, '子栏目一1', '这个是分类一', 5),
(15, 6, '顶级分类1', 1680956492, 1680956510, '分类关键词', '描述', 2);

-- --------------------------------------------------------

--
-- 表的结构 `ci_configu`
--

CREATE TABLE `ci_configu` (
  `id` smallint(8) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '变量的key',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(90) COLLATE utf8_unicode_ci NOT NULL COMMENT '变量的介绍说明',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '预留0，使用中1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_configu`
--

INSERT INTO `ci_configu` (`id`, `name`, `value`, `info`, `status`) VALUES
(1, 'web_name', '开发编程', '开发者社区，开发编程写代码', 1),
(2, 'web_basehost', 'http://vcncn.cn', '站点根目录', 1),
(3, 'web_keywords', 'seokey123', 'seo 关键词', 1),
(4, 'web_description', 'web_description', 'seo 描述', 1),
(5, 'web_icp', 'icp12123', 'ICP备案号', 1),
(6, 'cfg_imgtype', 'jpg|gif|png', '图片浏览器文件类型', 0),
(7, 'cfg_notallowstr', '非典|新冠|艾滋病|阳痿', '禁用词语（系统将直接停止用户动作）<br/>用|分开，但不要在结尾加|', 0),
(8, 'QQ_APP_KEY', '', '', 0),
(9, 'baidu_site_url', 'bd789.cn', '百度推送的网站url,主域名', 1),
(10, 'baidu_token', 'token11123', '百度推送的token', 1);

-- --------------------------------------------------------

--
-- 表的结构 `ci_sgpage`
--

CREATE TABLE `ci_sgpage` (
  `id` smallint(6) NOT NULL,
  `cid` smallint(6) NOT NULL DEFAULT '0' COMMENT '预留扩展分类',
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seo 描述',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '正文',
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '封面图',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `isdel` tinyint(1) NOT NULL DEFAULT '0' COMMENT '软删除，删除1，默认0',
  `click` int(10) NOT NULL DEFAULT '0' COMMENT '浏览点击数'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_sgpage`
--

INSERT INTO `ci_sgpage` (`id`, `cid`, `title`, `subtitle`, `description`, `content`, `image`, `create_time`, `update_time`, `isdel`, `click`) VALUES
(1, 1, '关于我们', '公司的简介页面', 'seo 描述', '开发者社区，开发编程学习，codeit.org.cn ,一起研究技术的博客', '', 1682334211, 1683884736, 0, 0),
(2, 2, '版权声明', '版权的免责声明', '描述', '版权说明，2222', '0', 1683884457, 1683884816, 0, 0),
(3, 0, '添加3', '333', '333', '3333', '0', 1683884887, 1683884898, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ci_stow`
--

CREATE TABLE `ci_stow` (
  `id` int(10) UNSIGNED NOT NULL,
  `mid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '会员id',
  `aid` mediumint(8) NOT NULL COMMENT '收藏的作品id',
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '收藏类型:1文章,2图书小说,3商品',
  `create_time` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `ci_tags`
--

CREATE TABLE `ci_tags` (
  `id` int(11) NOT NULL,
  `name` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `ci_tags`
--

INSERT INTO `ci_tags` (`id`, `name`, `status`) VALUES
(1, 'bd789.cn', 1),
(2, 'codeit', 1),
(3, '编程社区', 1),
(4, '测试标11', 0),
(5, '测试2', 1),
(6, '测试标签5', 0);

-- --------------------------------------------------------

--
-- 表的结构 `ci_uploads`
--

CREATE TABLE `ci_uploads` (
  `id` int(11) NOT NULL,
  `path` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'uploads后面的路径部分',
  `aid` int(10) NOT NULL DEFAULT '0' COMMENT '所属文章id',
  `title` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT '图片简单描述标注',
  `create_time` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '文章1,单页2，小说3'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `ci_articles`
--
ALTER TABLE `ci_articles`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_bookclass`
--
ALTER TABLE `ci_bookclass`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_books`
--
ALTER TABLE `ci_books`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_category`
--
ALTER TABLE `ci_category`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_configu`
--
ALTER TABLE `ci_configu`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_sgpage`
--
ALTER TABLE `ci_sgpage`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_stow`
--
ALTER TABLE `ci_stow`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_tags`
--
ALTER TABLE `ci_tags`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `ci_uploads`
--
ALTER TABLE `ci_uploads`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `ci_articles`
--
ALTER TABLE `ci_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用表AUTO_INCREMENT `ci_bookclass`
--
ALTER TABLE `ci_bookclass`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `ci_books`
--
ALTER TABLE `ci_books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用表AUTO_INCREMENT `ci_category`
--
ALTER TABLE `ci_category`
  MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- 使用表AUTO_INCREMENT `ci_configu`
--
ALTER TABLE `ci_configu`
  MODIFY `id` smallint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用表AUTO_INCREMENT `ci_sgpage`
--
ALTER TABLE `ci_sgpage`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `ci_stow`
--
ALTER TABLE `ci_stow`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `ci_tags`
--
ALTER TABLE `ci_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `ci_uploads`
--
ALTER TABLE `ci_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
